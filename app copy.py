
import sqlite3

# def generate_db():
conn = sqlite3.connect('test.db')
cursor = conn.cursor()
sqlstr = '''DROP TABLE IF EXISTS COMPANY'''
cursor.execute(sqlstr)
# 建立一個資料表
sqlstr2 =  '''CREATE TABLE COMPANY
    (ID INT PRIMARY KEY     NOT NULL,
    NAME           TEXT    NOT NULL,
    AGE            INT     NOT NULL,
    ADDRESS        CHAR(50),
    SALARY         REAL);'''
cursor.execute(sqlstr2)
conn.commit()
conn.close()
print("Now create table...")
# return "Do you ever shine?"

# if __name__ == '__main__':
# 	print("...")
# 	print("Now create table...")
#     test = generate_db()
#     test