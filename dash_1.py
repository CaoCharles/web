# -*- coding: utf-8 -*-
import dash
import dash_core_components as dcc
import dash_html_components as html

# css 放在assets資料夾底下，dash會自己抓取。
# external_stylesheets = ['https://codepen.io/chriddyp/pen/bWLwgP.css']

app = dash.Dash(__name__)

app.layout = html.Div(children=[
    html.H1(children='這是H1大標題'),
    html.H2(children='這是H2中標題'),
    html.Div(children='''
        Dash: A web application framework for Python.
    '''),
    html.Div(children='''
        Dash: A web application framework for Python.
    '''),
    dcc.Graph(
        id='example-graph',
        figure={
            'data': [
                {'x': [1, 2, 3], 'y': [4, 1, 2], 'type': 'bar', 'name': 'SF'},
                {'x': [1, 2, 3], 'y': [2, 4, 5], 'type': 'bar', 'name': u'Montréal'},
            ],
            'layout': {
                'title': 'Dash Data Visualization'
            }
        }
    )
])

if __name__ == '__main__':
    app.run_server(debug=True)