
from flask import Flask, g, request, render_template, redirect, url_for, flash
from flask_bootstrap import Bootstrap
import sqlite3

app = Flask(__name__)
bootstrap = Bootstrap(app)

DATABASE = './db/database.db'

def get_db():
    db = getattr(g, '_database', None)
    if db is None:
        df = g._database = sqlite3.connect(DATABASE)
    return db

@app.teardown_appcontext
def close_connection(exception):
    db = getattr(g, '_database', None)
    if db is not None:
        db.close()

@app.route('/')
def index():
    cur = get_db().cursor()

@app.route('/login', methods=['GET', 'POST'])
def login():
    #  利用request取得使用者端傳來的方法為何
    if request.method == 'POST':
                          #  利用request取得表單欄位值
        return 'Hello ' + request.values['username'] + '，你想吃的是' + request.values['meals'] + '，總共是$' + request.values['amount'] + '元。'

    #  非POST的時候就會回傳一個空白的模板
    return render_template('login.html')


# @app.route('/loginurl', methods=['GET', 'POST'])
# def login():
#     if request.method == 'POST':
#         if login_check(request.form['username'], request.form['password']):
#             flash('Login Success!')
#             return redirect(url_for('hello', username=request.form.get('username')))
#     return render_template('login.html')


# def login_check(username, password):
#     """登入帳號密碼檢核"""
#     if username == 'admin' and password == 'hello':
#         return True
#     else:
#         return False


# @app.route('/hello/<username>')
# def hello(username):
#     return render_template('hello.html', username=username)


if __name__ == '__main__':
    app.debug = True
    # app.secret_key = "123456"
    app.run()
