# -*- coding: utf-8 -*-
import dash
import dash_html_components as html

# css 放在assets資料夾底下，dash會自己抓取。
# external_stylesheets = ['https://codepen.io/chriddyp/pen/bWLwgP.css']

app = dash.Dash(__name__)

app.layout = html.Div([
    html.Div('MILKTEA', className='mark'),
    html.Div(children=[
        html.H2([
            '曹立諭',
            html.Span('　(CaoCharles)') 
            ]),
        html.H5('數經飲料部-奶茶品嘗師'),
        html.Hr(),
        html.P('創立數經飲料部，負責挑選每日奶茶店。喜好在優閒午後，探索巷弄間獨特的店家，致力於尋找人生中每個當下最適合的奶茶'),
        html.Div(className='circle circle1'),
        html.Div(className='circle circle2'),
    ],className = 'namecard'),
    html.H3('MILK TEA SCIENTIST', className='page_title')
])

if __name__ == '__main__':
    app.run_server(debug=True)